package gui;


import controller.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

public class Main {

    public static void main(String[] args) {
        String url = "http://localhost:5004";
        Client client = ClientBuilder.newClient();
        WelcomeController welcomeController = new WelcomeController(client, url);
    }
}
